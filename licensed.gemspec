Gem::Specification.new do |s|
  s.name        = 'licensed'
  s.version     = '0.4.3'
  s.date        = '2013-04-08'
  s.summary     = "Gets licenses of included gems"
  s.description = "A tool to find the licenses of all the gems being used in your app."
  s.authors     = ["Evan Hemsley"]
  s.email       = 'evan.hemsley@gmail.com'
  s.files       = ["lib/get_licenses.rb", "bin/licenses"]
  s.license     = "MIT"
  s.executables << 'licenses'
  s.homepage    =
    'https://gitlab.com/ehemsley/licensed'
end
