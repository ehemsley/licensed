module GetLicenses
  def self.get_licenses
    gem_list = []
    File.open("Gemfile.lock") do |file|

      line = ""
      while line.strip != "specs:"
        line = file.gets
      end

      while line = file.gets
        if (line == "\n")
          break;
        end

        list = line.strip.split
        gem_name = list.first
        gem_list << gem_name
      end
    end

    gem_list = gem_list.uniq
    license_types = get_license_types(gem_list)
    license_specified = license_types[0]
    custom_license = license_types[1]
    no_license = license_types[2]

    STDOUT.write "*** THE FOLLOWING GEMS HAVE SPECIFIED A LICENSE ***\n\n"
    license_specified.each do |gemLicense|
      STDOUT.write "#{gemLicense[0]}: #{gemLicense[1]}\n"
    end
    
    STDOUT.write "\n*** THE FOLLOWING GEMS INCLUDED THE TEXT OF A LICENSE ***\n\n"
    custom_license.each do |gemLicense|
      STDOUT.write "#{gemLicense[0]}:\n #{gemLicense[1]}\n"
    end

    STDOUT.write "\n*** THE FOLLOWING GEMS HAVE NOT SPECIFIED A LICENSE ***\n\n"
    no_license.each do |gem|
      STDOUT.write "#{gem}\n"
    end
  end

  def self.get_license_types(gem_list)
    license_types = []

    license_specified = []
    custom_license = []
    no_license = []

    gem_list.uniq.each do |gem|
      spec = Gem::Specification.find_by_name(gem)
      if !spec.licenses.empty?
        license_specified << [gem, spec.licenses.join(", ")]
      else
        path = spec.full_gem_path
        if File.exist?(path + "/LICENSE")
          license_file = File.open(path + "/LICENSE")
          custom_license << [gem, license_file.read]
        elsif File.exist?(path + "/MIT-LICENSE")
          license_file = File.open(path + "/MIT-LICENSE")
          custom_license << [gem, license_file.read]
        elsif File.exist?(path + "/MIT-LICENSE.txt")
          license_file = File.open(path + "/MIT-LICENSE.txt")
          custom_license << [gem, license_file.read]
        elsif File.exist?(path + "/LICENSE.txt")
          license_file = File.open(path + "/LICENSE.txt")
          custom_license << [gem, license_file.read]
        elsif File.exist?(path + "/LICENSE.rdoc")
          license_file = File.open(path + "/LICENSE.rdoc")
          custom_license << [gem, license_file.read]
        else
          no_license << gem
        end
      end
    end 

    license_types << license_specified
    license_types << custom_license
    license_types << no_license

    license_types
  end
end
